import Konva from 'konva'

export default class Text {
  constructor(stage, title,  desc, color , grid){
    const self = this;
    this.stage = stage;
    this.objs = [];
    this.title = title;
    this.desc = desc;
    this.color = color;
    this.grid = grid;
    this.layer = new Konva.Layer();
  }
  get(){
    return this.color;
  }
  changeTitle(t){
    this.title = t;
  }
  changeDesc(t){
    this.desc = t;
  }
  draw(){
    const self = this;
    let opt = {
      x: self.grid.point[0],
      y: self.grid.point[1],
      text: self.title.toUpperCase().replace( /\s\s+/g, ' ' ),
      fontFamily: 'Conv_Proxima Nova Bold',
      fontSize: self.grid.point[1]*1.5,
      width: self.grid.getCoords().width,
      id: 'title'
      //wrap: 'char'
    };

    if(chance.bool()){
      opt.fill = self.color
    }else{
      opt.stroke = self.color
      opt.strokeWidth = 2
      opt.fontStyle = 'bold'

      opt.fill = 'rgba(0,0,0,0)'
    }

    if( self.desc.replace( /\s\s+/g, ' ' ).length > 60 ){
      let descSM = {
        x: self.grid.point[0] ,
        y: self.stage.getHeight() - self.grid.point[1]*2,
        text: self.desc.replace( /\s\s+/g, ' ' ).slice(0,60), /// !!! тут в props опрокидываем текст
        fontSize: self.grid.point[1]/4,
        fontFamily: 'Conv_Proxima Nova Regular',
        fill: self.color,
        width: self.grid.getCoords().width/2,
        id: 'desc'
        //wrap: 'char'
      };
      let descSM2 = {
        x: self.grid.getCoords().width/2 + self.grid.point[0] ,
        y: self.stage.getHeight() - self.grid.point[1]*2,
        text: self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
        fontSize: self.grid.point[1]/4,
        fontFamily: 'Conv_Proxima Nova Regular',
        fill: self.color,
        width: self.grid.getCoords().width/2,
        id: 'desc2'
        //wrap: 'char'
      };
      self.objs.push( new Konva.Text(descSM) )
      self.objs.push( new Konva.Text(descSM2) )
    }else{
      let descSM = {
        x: chance.pick([self.grid.point[0] , self.grid.getCoords().width/2 + self.grid.point[0] ]),
        y: self.stage.getHeight() - self.grid.point[1]*2,
        text: self.desc.replace( /\s\s+/g, ' ' ), /// !!! тут в props опрокидываем текст
        fontSize: self.grid.point[1]/4,
        fontFamily: "Helvetica",
        fill: self.color,
        width: self.grid.getCoords().width/2,
        id: 'desc'
        //wrap: 'char'
      };
      self.objs.push( new Konva.Text(descSM) )
    }

    self.objs.push( new Konva.Text(opt) )
    //self.objs.push( new Konva.Text(descSM) )
    for(var i in this.objs){
      this.layer.add(this.objs[i]);
    }
    return this.layer;
  }
}
