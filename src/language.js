import LocalizedStrings from 'react-localization'

import logo from './pic/logo-11.png'
import logoen from './pic/logo_eng.png'

let strings = new LocalizedStrings({
 en:{
   eventname: "event's name",
   eventdesc: "more info",
   eventplace: "event's description",
   eventtime: "event's time",
   wanna: "get this",
   name: "plakot",
   tizer: "can't make poster? don't worry, generates himself here, just input your info.",
   notfound: "not this plakot anymore",
   thanks: "awesome! your plakot send on your mail! ",
   fail: ":( something went wrong",
   tir: "printing",
   adress: "your adress",
   youname: "your name",
   kitname: "your kitten name",
   mail: "your mail",
   print: "i'd like full construction",
   buy: "buy",
   details: [
     "price 1$",
     "resolution 300dpi",
     "size 70x100cm",
     "bleed 4mm",
     "+ design for groups in social networks"
   ],
   next: "next",
   back: "back",
   logo: logoen
 },
 it: {
   how:"Come vuoi il tuo uovo oggi?",
   boiledEgg:"Uovo sodo",
   softBoiledEgg:"Uovo alla coque",
   choice:"Come scegliere l'uovo"
 },
 ru: {
   eventname: "название мероприятия",
   eventdesc: "дополнительня информация",
   eventplace: "введите место",
   eventtime: "введите время",
   wanna: "хочу этот",
   name: "плакот",
   tizer: "не получается плакат? не нужно плакать, здесь он сам генерируется, просто введи текст.",
   notfound: "такого плакота не существует",
   thanks: "спасибо! ваш плакот прийдёт на почту в течении часа! ",
   fail: ":( что-то пошло не так",
   tir: "тираж",
   adress: "ваш адрес",
   youname: "ваше имя",
   kitname: "имя вашего кота",
   mail: "ваш email",
   print: "хочу, чтобы распечатали и привезли домой",
   buy: "купить",
   details: [
     "цена 99р",
     "разрешение 300dpi",
     "размер 70x100cm",
     "выпуск под обрез 4mm",
     "+ оформление для групп ВК/ФБ"
   ],
   next: "другой",
   back: "прошлый",
   logo
 }
});

// console.log(
//   strings.getLanguage()
// );


export default strings;
