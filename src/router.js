export default {
    param: function(object, base){
      var queryString = [];
      Object.keys(object).forEach(function (key) {
          var result,
              value;
          value =  object[key];
          if (base) {
              key = base + '[' + key + ']';
          }
          switch (typeof value) {
              case 'object':
                  result = Object.encodeToQueryString(value, key);
                  break;
              case 'array':
                  var qs = {};
                  value.forEach(function (val, i) {
                      qs[i] = val;
                  });
                  result = Object.encodeToQueryString(qs, key);
                  break;
              default:
                  result = key + '=' + encodeURIComponent(value);
          }

          if (value != null) {
              queryString.push(result);
          }
      });
      return queryString.join('&');
    },
    set: function(query){
      if (history.pushState) {
        var newurl = window.location.protocol + "//" + window.location.host + window.location.pathname + '?' + this.param(query);
        history ? history.pushState({path:newurl},'',newurl) : window.history.pushState({path:newurl},'',newurl);
      }
    },
    get: function(){
      var urlParams;
      (window.onpopstate = function () {
          var match,
              pl     = /\+/g,  // Regex for replacing addition symbol with a space
              search = /([^&=]+)=?([^&]*)/g,
              decode = function (s) { return decodeURIComponent(s.replace(pl, " ")); },
              query  = window.location.search.substring(1);

          urlParams = {};
          while (match = search.exec(query))
             urlParams[decode(match[1])] = decode(match[2]);
      })();
      return urlParams;
    },
    put: function(obj){
      this.data? this.data = this.get() : void(0);
      for (var i in obj){
        this.data[i] = obj[i];
        this.set(this.data);
      }
    },
    data: {}
}
