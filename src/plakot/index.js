import Konva from 'konva'
import Chance from 'chance'
import MD5 from '../md5'

import is from 'is_js'

//import Fill from './back'
import Grid from './grid'
//import Color from './colors'
//import Text from './text'
//import Shape from './shape'

import dao from './dao'
import limits from '../limits'

// андрес гурски !tile maps

const chance = new Chance()


export default function draw(self, json){

  if(!self) throw console.error("вызванно без this");

  self.colorlib = ["#000000","#FFFFFF","#FC1476","#F44336","#9C27B0","#2196F3","#673AB7","#00BCD4","#009688","#4CAF50","#8BC34A","#CDDC39","#FFEB3B","#FFC107","#FFC107","#FF5722","#795548","#03A9F4","#607D8B","#9E9E9E"];


   //!self.title ? self.title = '' : self.title = self.title.substring(0,limits.lentitle)
  if( !self.title ) self.title = ''
  if( !self.desc  ) self.desc = ''
  if( !self.time  ) self.time = ''
  if( !self.place ) self.place = ''


  const size = [
    is.mobile() ? window.innerWidth*.7 : window.innerHeight*.63,
    is.mobile() ? window.innerWidth : window.innerHeight*.9
  ]

  const stage = json ? Konva.Node.create(json, 'plakot') : new Konva.Stage({
    container: 'plakot',   // id of container <div>
    width:  self.sizex || size[0] ,
    height: self.sizey || size[1]
  });

  if(!json){
    console.log("generate new poster");

    const grid = new Grid(stage, chance);

    //const colors = new Color( chance.integer({min: 1 , max: 255 }) ,'hard', chance.pick(['triade', 'tetrade', 'analogic','mono']) );

    // for(let i=0;i<10;i++){
    //   let watermark = new Konva.Text({
    //     x: chance.integer({min: 0 , max: stage.getWidth() }),
    //     y: chance.integer({min: 0 , max: stage.getHeight() }),
    //     text: 'Made with Plakot generator :)',
    //     fontSize: chance.integer({min: 1 , max: 25 }),
    //     fontFamily: 'Helvetica',
    //     fill: 'blue',
    //     opacity: .15
    //   });
    //
    //   layer.add(watermark)
    // }

    //const textFill = chance.pick( colors.get().slice( colors.get().length  - 1 , colors.get().length ) );

    //const back = new Fill(stage ,  colors.get()[ chance.integer({min: 1 , max: 255 }) % colors.get().length ] );
    //const text = new Text(stage, self.title , self.desc , ['white','black'][ chance.integer({min: 1 , max: 255 }) % 2] , grid);
    //const shape = new Shape(stage , chance , grid, colors);

    // for(var i=0;i<chance.integer({min:0,max:40});i++){
    //   var imageObj = new Image();
    //     imageObj.onload = function() {
    //       var yoda = new Konva.Image({
    //         x: chance.pickone(  grid.getCoords().canX ),
    //         y: chance.pickone(  grid.getCoords().canY ),
    //         image: imageObj,
    //         width: 106,
    //         height: 118
    //       });
    //       // add the shape to the layer
    //       layer.add(yoda);
    //       // add the layer to the stage
    //       stage.add(layer);
    //     };
    //     imageObj.src = 'http://46.101.126.64:9000/'+chance.word({length: chance.integer({min:0,max:18}) });
    // }

    //console.log( grid.getCoords() );


    //stage.add(  back.draw() )
    //stage.add( shape.draw() )
    //stage.add(  text.draw() )
    const picker = dao()
    const rkey = chance.pick( Object.keys( picker ) );
    console.log("pink the " + rkey);

    stage.add(  picker[ rkey ]( self, stage, grid, chance) );


    //stage.add( grid.draw() )
    return self.pdf ? stage.toDataURL('image/jpeg') : JSON.parse( stage.toJSON() );
  }else{
    console.log("draw with json");
    //console.log(json);
    return stage.toDataURL('image/jpeg');
  }
}
