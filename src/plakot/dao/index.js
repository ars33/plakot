import postkod from './postkod'
import blur from './blur'
import sqser from './sqser'
import romb from './romb'
import tv from './tv'
import moon from './moon'
import mccartney from './mccartney'
import beach from './beach'
import pussy from './pussy'
import glitch from './glitch'
import ditegrapf from './ditegrapf'
import jeff from './jeff'
import isi from './isi'
import daivinchik from './daivinchik'
import myself from './myself'
import again from './again'
import weow from './weow'
import devo from './devo'
import twf from './twf'


export default (self)=> {
  if(!self){
    return {
      postkod,
      blur,
      sqser,
      romb,
      tv,
      moon,
      mccartney,
      beach,
      pussy,
      glitch,
      ditegrapf,
      jeff,
      isi,
      daivinchik,
      myself,
      again,
      weow,
      devo,
      twf
    }
  }else{

  }
}
