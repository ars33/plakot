import Konva from 'konva'
import splice from '../algo/splice'
import fill from '../algo/fill'

export default function(self, stage, grid, chance){
  const layer = new Konva.Layer();

  const pallete = self.colorlib;

  const color = splice( pallete , 1)

  const backgroundColor = splice( pallete );
  let foregroundColor = splice( pallete );


  fill(backgroundColor, layer,stage)


  const rect1 = new Konva.Rect({
      x: 0,
      y: 0,
      width: stage.getWidth(),
      height: grid.getCoords().pinkY(6,4 ,1),
      fill: color
  })
  layer.add(rect1)

  const cr = new Konva.Circle({
          x: stage.getWidth()/2,//grid.getCoords().canX[0] + grid.getCoords().marginY/2  + k ,
          y: stage.getWidth()/2,//grid.getCoords().canY[0] + grid.getCoords().marginY/2 + k ,
          radius: stage.getWidth()/2 -  grid.getCoords().pinkX(0,0 ,1),
          fill: backgroundColor
      })
      layer.add(cr)


      let cr1 = new Konva.Circle({
              x: 0,//grid.getCoords().canX[0] + grid.getCoords().marginY/2  + k ,
              y: stage.getWidth()/2,//grid.getCoords().canY[0] + grid.getCoords().marginY/2 + k ,
              radius: stage.getWidth()/2 -  grid.getCoords().pinkX(0,0 ,1),
              fill: splice( pallete ),
              opacity: .8
          })
          layer.add(cr1)
          cr1 = new Konva.Circle({
                  x: stage.getWidth(),//grid.getCoords().canX[0] + grid.getCoords().marginY/2  + k ,
                  y: stage.getWidth()/2,//grid.getCoords().canY[0] + grid.getCoords().marginY/2 + k ,
                  radius: stage.getWidth()/2 -  grid.getCoords().pinkX(0,0 ,1),
                  fill: splice( pallete ),
                  opacity: .8
              })
              layer.add(cr1)




  const tm = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[7] ,
    text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'time'
  })

  layer.add(tm)

  const pl = new Konva.Text({
    x: grid.getCoords().canX[1] ,
    y: grid.getCoords().canY[7] ,
    text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'place'
  })

  layer.add(pl)

  const dp = new Konva.Text({
    x: grid.getCoords().canX[2] ,
    y: grid.getCoords().canY[7] ,
    text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width*2 ,//+ grid.getCoords().magrinX ,//+ grid.getCoords().mrX ,
    height: grid.getCoords().height,
    name: 'desc'
    //align: 'right'
  })

  layer.add(dp)

  const T = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[5] + grid.getCoords().height/2 + grid.getCoords().mrY ,
    text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height,
    fontFamily: 'Conv_Proxima Nova Bold',
    fill: color,
    width: grid.getCoords().width*4 + grid.getCoords().marginX*3,
    name: "title"
  })

  layer.add(T)

  return layer;
}
