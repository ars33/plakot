import ColorScheme from 'color-scheme'

var colors = [
  ['#1e00f7'],
	['#ff0075'],
	['#e8453b', '#f1e43d', '#81b441'],
	['#823e90'],
	['#b4dfe9'],
	['#1ab7d1', '#1f99c9', '#f1e43d', '#81b441'],
	['#fcc00d'],
	['#594796', '#1db4d0'],
	['#ff0000', '#a50000'],
	['#00a37c'],
	['#ba0f55', '#ffb6fa'],
	['#001fff', '#5a85ff', '#bdcbff'],
	['#6e1bce', '#6e1bce'],
	['#077556'],
	['#ff00ff', '#ff00b7']
]

export default class Color{
  constructor(hue = 21, variation = 'soft', scheme = 'triade'){
    this.scheme = new ColorScheme;
    this.scheme.from_hue(hue).scheme(scheme).variation(variation);
  }
  get(){
    // let c = [];
    // for(var i in this.scheme.colors()){
    //   c.push( '#'+this.scheme.colors()[i] )
    // }
    return colors[Math.floor(Math.random() * colors.length)];
  }
}
