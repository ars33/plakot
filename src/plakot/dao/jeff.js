import Konva from 'konva'
import splice from '../algo/splice'
import fill from '../algo/fill'
import {rbtriangle} from '../algo/triangle'

export default function(self, stage, grid, chance){
  const layer = new Konva.Layer();

  let pallete = self.colorlib;

  const color = splice( pallete , 1)
  console.log(pallete.length);

  let backgroundColor = splice( pallete );
  let foregroundColor = splice( pallete );
  console.log(pallete.length);


  fill(backgroundColor, layer, stage)


  for(let i=0;i<4;i++){

    let rect1;
    if(i<2){
      if(i==1){
        rect1 = new Konva.Rect({
          x: [0, stage.getWidth()/2][i%2],
          y: [0, stage.getWidth()/2][(i+1)%2],
          width: stage.getWidth()/2,
          height: stage.getWidth()/2,
          fill: splice( pallete )
        })
      }else{
        rect1 = new Konva.Circle({
          x: stage.getWidth()/4 + [0, stage.getWidth()/2][i%2],
          y: stage.getWidth()/4 + [0, stage.getWidth()/2][(i+1)%2],
          radius: stage.getWidth()/4,
          fill: splice( pallete )
        })
      }
    }else{
      rect1 = rbtriangle(
        [0, stage.getWidth()/2][(i+3)%2],
        [0, stage.getWidth()/2][(i+5)%2],
        stage.getWidth()/2, splice( pallete ) , chance.integer({min: 0, max: 3}) )
    }
    layer.add(rect1)
  }




  const tm = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[7] ,
    text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'time'
  })

  layer.add(tm)

  const pl = new Konva.Text({
    x: grid.getCoords().canX[1] ,
    y: grid.getCoords().canY[7] ,
    text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'place'
  })

  layer.add(pl)

  const dp = new Konva.Text({
    x: grid.getCoords().canX[2] ,
    y: grid.getCoords().canY[7] ,
    text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width*2 ,//+ grid.getCoords().magrinX ,//+ grid.getCoords().mrX ,
    height: grid.getCoords().height,
    name: 'desc'
    //align: 'right'
  })

  layer.add(dp)

  const T = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[5] + grid.getCoords().height/2 + grid.getCoords().mrY ,
    text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height,
    fontFamily: 'Conv_Proxima Nova Bold',
    fill: color,
    width: grid.getCoords().width*4 + grid.getCoords().marginX*3,
    name: "title"
  })

  layer.add(T)

  return layer;
}
