import { createStore , combineReducers } from 'redux'

import initialState from './initialState'


const _extends = Object.assign
// actions

function counter(state = 0, action) {
  console.log(state);
  switch (action.type) {
  case 'INCREMENT':
    return state.screen + 1
  case 'DECREMENT':
    return state.screen - 1
  default:
    return state
  }
}


function counter2(state = 2, action) {
  switch (action.type) {
  case 'INC':
    return state + 1
  case 'DEC':
    return state - 1
  default:
    return state
  }
}


const App = combineReducers({
  counter,
  counter2
})

// Создаем хранилище Redux которое хранит состояние вашего приложения.
// Его API - { subscribe, dispatch, getState }.
let store = createStore(counter)

export default store
