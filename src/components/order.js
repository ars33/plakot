import React from 'react'
import styles from './order.styl'
import MD5 from '../md5'
import strings from '../language'
import StripeCheckout from 'react-stripe-checkout'

import lapa from '../pic/lapa.svg'
import bubl from '../pic/bubl.png'

import Rest from '../rest'
import router from '../router'

const R = new Rest()

export default class Order extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      need: false,
      link: this.props.link,
      name: '',
      kotname: '',
      email: '',
      ql: '',
      adress: '',
      lapa: 50
    }
    this.handleInputChange = this.handleInputChange.bind(this);

    this.inter = undefined;
  }
  renderField(){
    if(this.state.need) {return (
      <div>
        <br/>
        <input className={styles.input}
        value={this.state.ql}
        onChange={this.handleInputChange}
        name="ql" placeholder={strings.tir} />
        <br/>
        <input className={styles.input+ ' ' + styles.inpm}
        value={this.state.adress}
        onChange={this.handleInputChange}
        name="adress" placeholder={strings.adress} />
      </div>
    )} else {return <div></div>}
  }
  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      [name]: value
    });
  }
  handleSubmit(event) {
    yaCounter42977284.reachGoal('submit');
    if(strings.getLanguage() == 'ru' ){
      R.order(this.state).then( e=> window.location.href = e.data );
    }else{

    }
    event.preventDefault();
  }
  handleOpen(){
    this.inter  ? clearInterval(this.inter) : void(0);
    this.inter = setInterval( _=> {
      this.state.lapa > 0 ? this.setState({lapa: this.state.lapa - .5 }) : void(0)
      console.log(this.state.lapa);
    }, 10)
  }
  handleClose(){
    this.inter  ? clearInterval(this.inter) : void(0);
    this.inter = setInterval( _=> {
      this.state.lapa < 50 ?  this.setState({lapa: this.state.lapa + .5 }): void(0)
      console.log(this.state.lapa);
    }, 10)
  }
  componentWillUnmount(){
    this.inter ? clearInterval(this.inter) : void(0);
  }
  onToken = (token) => {
    R.stripe( Object.assign( token , this.state ) ).then(token => {
      //router.put({paid:"sucsess"})
      window.location.href = token;
    }).catch(e => window.location.href = "https://plakot.ru/?paid=fail" );
  }
  render(){
    const det = strings.details.map( (d)=>  <li key={d}><small>{d}</small></li> )

    let submit = <button type="submit" id='submit' className={styles.button} style={{background: `${this.props.border}`, color: 'white'}} >{strings.buy}</button>
    if(strings.getLanguage() != 'ru'){
      submit =
      <StripeCheckout
        name={strings.name}
        description="plagen"
        image={bubl}
        ComponentClass="div"
        panelLabel={strings.buy}
        amount={100}
        currency="USD"
        stripeKey="pk_live_oIbftdx5c4Ynf2FbgqEgMjyQ" // "pk_test_I5BLEE6fGvi0uqljoHB6ppqT"
        locale="en"
        email="parsenicum@gmail.com"
        billingAddress={false}
        zipCode={false}
        token={this.onToken}
        // Note: `reconfigureOnUpdate` should be set to true IFF, for some reason
        // you are using multiple stripe keys
        reconfigureOnUpdate={false}
        // Note: you can change the event to `onTouchTap`, `onClick`, `onTouchStart`
        // useful if you're using React-Tap-Event-Plugin
        triggerEvent="onClick"
        >
        <button className={styles.button} style={{background: `${this.props.border}`, color: 'white'}} >{strings.buy}</button>
      </StripeCheckout>
    }
    return(
      <div className={styles.main}>
        <img style={{ left: `-${this.state.lapa}vw` }} className={styles.lapa} src={lapa} />
        <div>
          <img className={styles.render + ' '+ styles.shadow} src={this.props.src} />
          <div className={styles.render +  ' ' + styles.border}>
            <ul className={styles.list}>{det}</ul>
          </div>
        </div>
        <form className={styles.order} onSubmit={this.handleSubmit.bind(this)}>

          <div
          onClick= { this.props.back }
          className={styles.back}
          >✕</div>

          <input type="hidden"
          name="link"
          value={this.state.link}
          onChange={this.handleInputChange}
          />

          <input className={styles.input}
          value={this.state.name}
          onChange={this.handleInputChange}
          name="name" placeholder={strings.youname} />


          <input className={styles.input + ' ' + styles.inpm}
          value={this.state.kotname}
          onChange={this.handleInputChange}
          onFocus={ this.handleOpen.bind(this) }
          onBlur={ this.handleClose.bind(this) }
          name="kotname" placeholder={strings.kitname} />


          <input className={styles.input+ ' ' + styles.inpm}
              value={this.state.email}
              onChange={this.handleInputChange}
              name="email" placeholder={strings.mail} />

          <div className={styles.dop} >
            <div
              name="need"
              type="checkbox"
              className={styles.round}
              onClick={_=> this.setState({need: !this.state.need})}
              style={{background: this.state.need ? 'black':'white'}}
            ></div>
            {strings.print}
          </div>
          {this.renderField()}
          {submit}
        </form>
      </div>
    )
  }
}
