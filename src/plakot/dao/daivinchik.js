// стандартный шаблон плаката

import Konva from 'konva'
import fill from '../algo/fill'
import splice from '../algo/splice'
import { rbstriangle } from '../algo/triangle'
import convert from 'color-convert'

export default function(self, stage, grid, chance){
  const layer = new Konva.Layer();

  let clib = self.colorlib

  const color = clib.splice( chance.integer({min:0,max:1}) ,1)[0]


  fill(clib.splice( chance.integer({min:0,max:clib.length-1}) ,1)[0], layer, stage) //chance.pick( self.colorlib.slice(2, self.colorlib.length) )

  console.log(clib.length);

  const tm = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[0] ,
    text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width*2,
    height: grid.getCoords().height,
    name: 'time'
  })

  layer.add(tm)

  const pl = new Konva.Text({
    x: grid.getCoords().canX[2] ,
    y: grid.getCoords().canY[0] ,
    text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'place'
  })

  layer.add(pl)

  const dp = new Konva.Text({
    x: grid.getCoords().canX[3] ,
    y: grid.getCoords().canY[0] ,
    text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    align: 'right',
    name: 'desc'
  })

  layer.add(dp)

    const Blsh = new Konva.Text({
      x: grid.getCoords().canX[0] ,
      y: grid.getCoords().canY[1] ,
      text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
      fontSize: grid.getCoords().height *2 ,
      fontFamily: 'Conv_Proxima Nova Bold',
      fill: color,
      width: grid.getCoords().width *4 + grid.getCoords().marginX*4,
      height: grid.getCoords().height,
      name: "title"
    })

    layer.add(Blsh)

    let rect = new Konva.Rect({
        x: stage.getWidth()*.1,
        y: stage.getHeight() - stage.getWidth() + stage.getWidth()*.1,
        width: stage.getWidth()*.8,
        height: stage.getWidth()*.8,
        stroke: splice(clib),
        strokeWidth: grid.getCoords().pinkX(0,2,0),
        opacity: .6
    })
    layer.add(rect)
    rect = new Konva.Circle({
        x: stage.getWidth()*.1 + stage.getWidth()*.4,
        y: stage.getHeight() - stage.getWidth() + stage.getWidth()*.1 + stage.getWidth()*.4,
        radius: stage.getWidth()*.4,
        stroke: splice(clib),
        strokeWidth: grid.getCoords().pinkX(0,2,0),
        opacity: .6
    })
    layer.add(rect)

    layer.add(
      rbstriangle(
        stage.getWidth()*.125,
        stage.getHeight() - stage.getWidth() + stage.getWidth()*.15,
        stage.getWidth()*.75,
        'rgba( '+convert.hex.rgb(splice(clib)).join(', ')+', .6)' ,
        2,
        grid.getCoords().pinkX(0,2,0)
      )
    )

    layer.add( rbstriangle(stage.getWidth()*.1, stage.getHeight() - stage.getWidth() + stage.getWidth()*.1, ) )



  return layer;

}
