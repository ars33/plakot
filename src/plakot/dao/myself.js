import Konva from 'konva'
import splice from '../algo/splice'
import fill from '../algo/fill'

export default function(self, stage, grid, chance){

  console.log("PROTOTYPE NAMED myself");

  const layer = new Konva.Layer();

  const pallete = self.colorlib;

  const color = splice( pallete , 1)

  const backgroundColor = splice( pallete );
  let foregroundColor = splice( pallete );


  fill(backgroundColor, layer, stage)


  const color2 = splice( pallete );


  const tm = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[7] ,
    text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'time'
  })

  layer.add(tm)

  const pl = new Konva.Text({
    x: grid.getCoords().canX[1] ,
    y: grid.getCoords().canY[7] ,
    text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'place'
  })

  layer.add(pl)

  const dp = new Konva.Text({
    x: grid.getCoords().canX[2] ,
    y: grid.getCoords().canY[7] ,
    text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width*2 ,//+ grid.getCoords().magrinX ,//+ grid.getCoords().mrX ,
    height: grid.getCoords().height,
    name: 'desc'
    //align: 'right'
  })

  layer.add(dp)


  const ql = 7
  for(var i=(ql-1);i>=0;i--){
    let k = stage.getWidth()/ql*(Math.pow(i,.2));
    let pad = stage.getWidth()*(Math.pow(i,1))/(ql) - stage.getWidth()/ql;
    const r = new Konva.Rect({
      width: self.title.length*Math.pow(k,.9) + grid.getCoords().canX[0] ,
      height: k,
      x: 0 ,
      y: pad,
      fill: color
    })
    const T = new Konva.Text({
      x: grid.getCoords().canX[0] ,
      y: pad,//+ grid.getCoords().height/2 + grid.getCoords().mrY ,
      text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
      fontSize: k,
      fontFamily: 'Conv_Proxima Nova Bold',
      fill: backgroundColor,
      //width: grid.getCoords().width*4 + grid.getCoords().marginX*3,
      name: "title"
    })

    console.log(T.getWidth() );

    layer.add(r)
    layer.add(T)
  }

  return layer;
}
