// стандартный шаблон плаката

import Konva from 'konva'
import fill from '../algo/fill'
import splice from '../algo/splice'

export default function(self, stage, grid, chance){
  const layer = new Konva.Layer();

  let clib = self.colorlib

  const color = splice(clib,1)


  fill(clib.splice( chance.integer({min:0,max:clib.length-1}) ,1)[0], layer, stage) //chance.pick( self.colorlib.slice(2, self.colorlib.length) )

  console.log(clib.length);

  const tm = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[0] ,
    text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width*2,
    height: grid.getCoords().height,
    name: 'time'
  })

  layer.add(tm)

  const pl = new Konva.Text({
    x: grid.getCoords().canX[2] ,
    y: grid.getCoords().canY[0] ,
    text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'place'
  })

  layer.add(pl)

  const dp = new Konva.Text({
    x: grid.getCoords().canX[3] ,
    y: grid.getCoords().canY[0] ,
    text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    align: 'right',
    name: 'desc'
  })

  layer.add(dp)

    // const Blsh = new Konva.Text({
    //   x: grid.getCoords().canX[0] ,
    //   y: grid.getCoords().canY[1] ,
    //   text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    //   fontSize: stage.getWidth()/2 ,
    //   fontFamily: 'Conv_Proxima Nova Bold',
    //   fill: color,
    //   width: stage.getWidth(),
    //   height: stage.getWidth(),
    //   name: "title"
    // })
    //
    // layer.add(Blsh)

    const cs = [splice(clib),splice(clib)]

    for(var i=0;i<4;i++){
      console.log(`${i>>1} ${i%2} = ${!(i%2 || i>>1) || (i%2 && i>>1)}`);
      const rect = new Konva.Rect({
            x: stage.getWidth()/2*(i%2),
            y: stage.getHeight() - stage.getWidth() + stage.getWidth()/2*(i>>1),
            width: stage.getWidth()/2,
            height: stage.getWidth()/2,
            fill: cs[  !(i%2 || i>>1) || (i%2 && i>>1) ? 1 : 0 ]
        })
        layer.add(rect)

        const Blsh = new Konva.Text({
          x: stage.getWidth()/2*(i%2) ,
          y: stage.getHeight() - stage.getWidth() + stage.getWidth()/2*(i>>1) ,
          text: self.title.toLowerCase().substring(i,i+1) ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
          fontSize: stage.getWidth()/2 ,
          fontFamily: 'Conv_Proxima Nova Bold',
          fill: color,
          align: "center",
          width: stage.getWidth()/2,
          height: stage.getWidth()/2,
          name: "title"
        })

        layer.add(Blsh)
    }

    // for(var i=0;i<5;i++){
    //   const col = clib.splice( chance.integer({min:0,max:clib.length-1}) ,1)[0]
    //   console.log(clib.length);
    //   const rect = new Konva.Rect({
    //       x: i/5 * stage.getWidth(),
    //       y: grid.getCoords().canY[2] + grid.getCoords().width *.5,
    //       width: stage.getWidth()/5,
    //       height: stage.getHeight(),
    //       fill: col
    //   })
    //   layer.add(rect)
    // }



  return layer;

}
