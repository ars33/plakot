import Konva from 'konva'
import splice from '../algo/splice'
import fill from '../algo/fill'
import {triangle} from '../algo/triangle'

export default function(self, stage, grid, chance){
  const layer = new Konva.Layer();

  const pallete = self.colorlib;

  const color = splice( pallete , 1)

  const backgroundColor = splice( pallete );
  let foregroundColor = splice( pallete );


  fill(backgroundColor, layer, stage)


  const color2 = splice( pallete );

  for(let i=0;i<9;i++){
    for(let k=0;k<9;k++){
      layer.add( triangle( stage.getWidth()/9, i*stage.getWidth()/9, k*stage.getWidth()/9, i+k > 7? color2 : color ) )
    }
  }






  const tm = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[7] ,
    text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'time'
  })

  layer.add(tm)

  const pl = new Konva.Text({
    x: grid.getCoords().canX[1] ,
    y: grid.getCoords().canY[7] ,
    text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'place'
  })

  layer.add(pl)

  const dp = new Konva.Text({
    x: grid.getCoords().canX[2] ,
    y: grid.getCoords().canY[7] ,
    text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width*2 ,//+ grid.getCoords().magrinX ,//+ grid.getCoords().mrX ,
    height: grid.getCoords().height,
    name: 'desc'
    //align: 'right'
  })

  layer.add(dp)

  const T = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[5] + grid.getCoords().height/2 + grid.getCoords().mrY ,
    text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height,
    fontFamily: 'Conv_Proxima Nova Bold',
    fill: color,
    width: grid.getCoords().width*4 + grid.getCoords().marginX*3,
    name: "title"
  })

  layer.add(T)

  return layer;
}
