import React from 'react'
import icons from './social.styl'
import image from './images'
import {
  FacebookButton, FacebookCount,
  PinterestButton, PinterestCount,
  VKontakteButton, VKontakteCount,
  TwitterButton, TwitterCount,
  TumblrButton, TumblrCount, GooglePlusButton, XingButton, EmailButton, RedditButton, LinkedInButton
} from "react-social";

// Эмиль рено фильмы
// антиутопия..
// Im not a robot повсюду развешивать

// томас руф...
// Джеф Уолл
// Контроьные отпечатки...

// Что можно сделать в обществе, уставшим от революций?

// Новая находка, новая идея... Гроу хаккинг.. Поиски..

// сделать стилистические другую кнопку back

export default class Social extends React.Component {
  componentDidMount(){

  }
  render(){
    const link = window.location.href, message = 'Конструктор плакатов';
    return(
        <details className={icons.over}>
          <summary>SHARE</summary>
          <div className={icons.shareblock}>
          <FacebookButton title="Share via Twitter" appId={"null"} message={message} url={link} element="a" className={icons.socail} >
             <img src={image.facebook}  className={icons.fa + ' ' +icons.facebook}/>
          </FacebookButton>
          <VKontakteButton title="Share via VKontakte" message={message} url={link} element="a" className={icons.socail}>
              <img src={image.vk}  className={icons.fa + ' ' +icons.vk}/>
          </VKontakteButton>
          <TwitterButton title="Share via Twitter" message={message} url={link} element="a" className={icons.socail}>
              <img src={image.twitter}  className={icons.fa + ' ' +icons.twitter}/>
          </TwitterButton>
          <GooglePlusButton title="Share via Google+" message={message} url={link} element="a" className={icons.socail}>
              <img src={image.google}  className={icons.fa + ' ' +icons.google}/>
          </GooglePlusButton>
          <PinterestButton title="Share via Pinterest" message={message} url={link} element="a" className={icons.socail}>
              <img src={image.pinterest}  className={icons.fa + ' ' +icons.pinteres}/>
          </PinterestButton>
          <LinkedInButton title="Share via Linkedin" message={message} url={link} element="a" className={icons.socail}>
              <img src={image.linkedin}  className={icons.fa + ' ' +icons.linkedin}/>
          </LinkedInButton>
          <RedditButton title="Share via Reddit" message={message} url={link} element="a" className={icons.socail}>
              <img src={image.reddit}  className={icons.fa + ' ' +icons.reddit}/>
          </RedditButton>
          <EmailButton title="Share via E-Mail" message={message} url={link} element="a" className={icons.socail}>
              <img src={image.at}  className={icons.fa + ' ' +icons.at}/>
          </EmailButton>
          <XingButton title="Share via Xing" message={message} url={link} element="a" className={icons.socail}>
              <img src={image.xing}  className={icons.fa + ' ' +icons.xing}/>
          </XingButton>
          <TumblrButton title="Share via Tumblr" message={message} url={link} element="a" className={icons.socail}>
              <img src={image.tumblr}  className={icons.fa + ' ' +icons.tumblr}/>
          </TumblrButton>
          </div>
        </details>
    )
  }
}
