import Konva from 'konva'

export default class Fill {
  constructor(stage, color){
    const self = this;
    this.stage = stage;
    this.objs = [];
    this.color = color;
    this.layer = new Konva.Layer();
  }
  get(){
    return this.color;
  }
  draw(){
    const self = this;
    self.objs.push( new Konva.Rect({
      width: self.stage.getWidth(),
      height: self.stage.getHeight(),
      fill:  self.color
    }))
    for(var i in this.objs){
      this.layer.add(this.objs[i]);
    }
    return this.layer;
  }
}
