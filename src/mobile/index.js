import React from 'react'
import styles from './mobile.styl'

import draw from '../plakot/'
import Rest from '../rest'
import router from '../router'

import logo from '../pic/logo.png'
import bubl from '../pic/bubl.png'
import lfing from '../pic/lfing.svg'

import strings from '../language'

import bck from '../pic/bck.png'
import nxt from '../pic/nxt.png'


const R = new Rest()

class St extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      value : ''
    }
  }
  render(){
    let el = <a href='#' className={styles.wanna} onClick={ this.props.buy }>{strings.wanna}</a>
    if (this.props.step) {
        el = <input
        onKeyPress={e=> {
          if(e.key==="Enter" ){
            this.props.plus()
            this.setState({value:''})
          }
        }}
        className={styles.inputb}
        value={this.state.value}
        onChange={(e)=> this.setState({value:e.target.value}, _=> {
          this.props.change(this.props.step, this.state.name )
        }) }
        placeholder={[strings.youname,strings.kitname,strings.mail,strings.print][this.props.step-6]}
        />
        if(this.props.step > 9){
          el = <a href='#' className={styles.wanna} onClick={ this.props.checkout }>{strings.buy}</a>
        }
    }
    return el;
  }
}


export default class Mbl extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      screen: 1,
      name: "",
      kotname: "",
      files: "",
      email: "",
      print: "",
      details: "",
      title: "",
      time: "",
      place: "",
      history: [],
      link: true,
      endx: window.innerWidth*.15 ,
      endy: (window.innerHeight - window.innerWidth)/2,
      sx: window.innerWidth*.15,
      sy: (window.innerHeight - window.innerWidth)/2,
      offsetx:0,
      offsety:0
    }
  }
  handleChange(n){
    this.setState({screen: this.state.screen + n})
  }
  handleInput(n , v){
    let key = ["name","kotname", "email", "print"][n-6]
    console.log(key);
    this.setState({[key]: v }, _=> console.log(this.state[key]))
  }
  checkout(){
    console.log("checkout");
    const state = this.state;
    state.history = 0;
    state.colorlib = 0;
    state.link = window.location.href;
    R.order(state).then(e=> window.location.href = e.data)
  }
  historySave(dt){ // сохрняем в локальную историю
    console.log("call");
    const newObj = { name:   Math.floor( Math.random() * 10 ) + '0' + (new Date()).getTime()  , data: dt}
    this.setState({
      history : this.state.history.concat(newObj)
    })
    return newObj.name;
  }
  back(){
    const self = this;
    console.log( this.state.history.length );
    if(self.state.history.length > 1){
      window.history.go(-1)
      this.setState({
        history: self.state.history.slice(0, self.state.history.length -1 )
      }, _=>  draw( this.state, this.state.history[this.state.history.length - 1 ].data ) )
    }else{
      this.setState({screen: 4})
    }
  }
  componentDidUpdate(prevProps,prevState){

    if(prevState.screen != this.state.screen && this.state.screen == 0){
      this.generate()
    }
    //this.save = setTimeout( this.redraw , 2500)
  }
  generate(){
    const self = this;
    router.put({seed: self.historySave(draw(self.state)) })
  }
  buy(){
    const self = this;
    this.setState({screen: 6});
    self.state.history.length ?
    R.add( self.state.history[self.state.history.length - 1 ].data )
     .then((d)=> {d.status==200 ? router.put({seed: d.data.name.split('.')[0] }) : void(0) })
     .catch(e=> console.log(e))
     : void(0);
  }
  details(det){
    return (
      <ul className={styles.details}>
        <h2>ЧЕК</h2>
        {det.name ? <li>имя: {det.name}</li> : ''}
        {det.kotname ? <li>имя кота: {det.kotname}</li> : ''}
        {det.email ? <li>email: {det.email}</li> : ''}
        {det.print ? <li>распечатать: {det.print}</li> : ''}
        <hr/>
        {det.screen > 9 ? <small>Итого: 99р</small> : '' }
      </ul>
    )
  }
  render(){
    const self = this;
    const numbers = [1, 2, 3, 4];
    const numbersOrder = [6, 7, 8, 9, 10];
    const colors = ['yellow', 'blue', 'magenta', 'red', 'blue', 'yellow']
    const checksOrder = numbersOrder.map((number) =>
      <div key={number} className={styles.check} style={{background: self.state.screen == number ? colors[number % colors.length] : 'black'}} onClick={_=> self.setState({screen: number})} ></div>
    );
    const checks = numbers.map((number) =>
      <div key={number} className={styles.check} style={{background: self.state.screen == number ? colors[number % colors.length] : 'black'}} onClick={_=> self.setState({screen: number})} ></div>
    );
    let el = <div></div>;
    switch (this.state.screen) {
      case 1: //название
        el =  <div className={styles.main}>
          <img src={strings.logo} className={styles.logosmall} />
          <div className={styles.inblock}>
            <img src={lfing} className={styles.bth + ' '  + styles.lbth} onClick={_=> this.setState({screen: this.state.screen-1})} />
            <input className={styles.input} value={this.state.title} onKeyPress={e=> e.key==="Enter"  ? this.setState({screen: this.state.screen +1}) : void(0) } onChange={(e)=> this.setState({title:e.target.value})} placeholder={strings.eventname} />
            <img src={lfing} className={styles.bth} onClick={_=> this.setState({screen: this.state.screen+1})} />
          </div>
          <div className={styles.checks}>{checks}</div>
        </div>
        break;
        case 2: // время
          el = <div className={styles.main}>
            <img src={strings.logo} className={styles.logosmall} />
            <div className={styles.inblock}>
              <img src={lfing} className={styles.bth + ' '  + styles.lbth} onClick={_=> this.setState({screen: this.state.screen-1})} />
              <input className={styles.input} onKeyPress={e=> e.key==="Enter"  ? this.setState({screen: this.state.screen +1}) : void(0) } value={this.state.time} onChange={(e)=> this.setState({time:e.target.value})} placeholder="время проведения"/>
              <img src={lfing} className={styles.bth} onClick={_=> this.setState({screen: this.state.screen+1})} />
            </div>
            <div className={styles.checks}>{checks}</div>
          </div>
          break;
          case 3: // место
            el = <div className={styles.main}>
              <img src={strings.logo} className={styles.logosmall} />
              <div className={styles.inblock}>
                <img src={lfing} className={styles.bth + ' '  + styles.lbth} onClick={_=> this.setState({screen: this.state.screen-1})} />
                <input className={styles.input} onKeyPress={e=> e.key==="Enter"  ? this.setState({screen: this.state.screen +1}) : void(0) } value={this.state.place} onChange={(e)=> this.setState({place:e.target.value})} placeholder="место" />
                <img src={lfing} className={styles.bth} onClick={_=> this.setState({screen: this.state.screen+1})} />
              </div>
              <div className={styles.checks}>{checks}</div>
            </div>
            break;
            case 4: // детали
              el = <div className={styles.main}>
                <img src={strings.logo} className={styles.logosmall} />
                <div className={styles.inblock}>
                  <img src={lfing} className={styles.bth + ' '  + styles.lbth} onClick={_=> this.setState({screen: this.state.screen-1})} />
                  <input className={styles.input}  onKeyPress={e=> e.key==="Enter"  ? this.setState({screen: 0}, _=> self.generate.bind(self) ) : void(0) } value={this.state.details} onChange={(e)=> this.setState({details:e.target.value})} placeholder="дополнительная информация" />
                  <img src={lfing} className={styles.bth} onClick={_=> this.setState({screen: 0}, _=> self.generate.bind(self) ) } />
                </div>
                <div className={styles.checks}>{checks}</div>
              </div>
              break;
                // case 6: //заказ имя
                //   el = <div className={styles.main}>
                //     <img src={strings.logo} className={styles.logosmall} />
                //     <div>{this.details(this.state)}</div>
                //     <div className={styles.inblock}>
                //       <img src={lfing} className={styles.bth + ' '  + styles.lbth} onClick={_=> this.setState({screen: 0})} />
                //     <input className={styles.input} value={this.state.name} onChange={(e)=> this.setState({name:e.target.value})} placeholder="ваше имя" />
                //     <img src={lfing} className={styles.bth} onClick={_=> this.setState({screen: this.state.screen+1})} />
                //   </div>
                //     <div className={styles.checks}>{checksOrder}</div>
                //   </div>
                //   break;
                //   case 7: //заказ имя кота
                //     el = <div className={styles.main}>
                //       <img src={strings.logo} className={styles.logosmall} />
                //       <div>{this.details(this.state)}</div>
                //       <div className={styles.inblock}>
                //         <img src={lfing} className={styles.bth + ' '  + styles.lbth} onClick={_=> this.setState({screen: this.state.screen-1})} />
                //         <input className={styles.input} value={this.state.kotname} onChange={(e)=> this.setState({kotname:e.target.value})} placeholder="имя вашего кота" />
                //         <img src={lfing} className={styles.bth} onClick={_=> this.setState({screen: this.state.screen+1})} />
                //       </div>
                //       <div className={styles.checks}>{checksOrder}</div>
                //     </div>
                //     break;
                //     case 8: //заказ email
                //       el = <div className={styles.main}>
                //         <img src={strings.logo} className={styles.logosmall} />
                //         <div>{this.details(this.state)}</div>
                //         <div className={styles.inblock}>
                //           <img src={lfing} className={styles.bth + ' '  + styles.lbth} onClick={_=> this.setState({screen: this.state.screen-1})} />
                //           <input className={styles.input} value={this.state.email} onChange={(e)=> this.setState({email:e.target.value})} placeholder="ваше email" />
                //           <img src={lfing} className={styles.bth} onClick={_=> this.setState({screen: this.state.screen+1})} />
                //         </div>
                //         <div className={styles.checks}>{checksOrder}</div>
                //       </div>
                //       break;
                //       case 9: //заказ печать
                //         el = <div className={styles.main}>
                //           <img src={strings.logo} className={styles.logosmall} />
                //           <div>{this.details(this.state)}</div>
                //           <div className={styles.inblock}>
                //             <img src={lfing} className={styles.bth + ' '  + styles.lbth} onClick={_=> this.setState({screen: this.state.screen-1})} />
                //             <input className={styles.input} value={this.state.print} onChange={(e)=> this.setState({print:e.target.value})} placeholder="надо ли распечатать?" />
                //             <img src={lfing} className={styles.bth} onClick={_=> this.setState({screen: this.state.screen+1})} />
                //           </div>
                //           <div className={styles.checks}>{checksOrder}</div>
                //         </div>
                //         break;
                //         case 10: //заказ атака
                //           el = <div className={styles.main}>
                //             <img src={strings.logo} className={styles.logosmall} />
                //             <div>{this.details(this.state)}</div>
                //             <a href='#' onClick={ this.checkout.bind(this) } className={styles.bth} >купить</a>
                //             <div className={styles.checks}>{checksOrder}</div>
                //           </div>
                //           break;
      default:
        el = <div className={styles.main}>
          <div id='plakot'></div>
          <div className={styles.bar}>
            <div className={styles.bck} onClick={ this.back.bind(this)} >{strings.back}</div>
            <div className={styles.nxt} onClick={ this.generate.bind(this)} >{strings.next}</div>
          </div>
          <St buy={this.buy.bind(this)} checkout={this.checkout.bind(this)} step={this.state.screen} plus={this.handleChange.bind(this,1)} change={this.handleInput.bind(this)} />
        </div>
    }
    return (
      <div>{el}</div>
    )
  }
}
