import Konva from 'konva'
import fill from '../algo/fill'
import splice from '../algo/splice'

import convert from 'color-convert'

export default (self, stage, grid, chance)=>{
  const layer = new Konva.Layer();


  const colorlib = self.colorlib;
  // step 1
  const fillColor = splice(colorlib,1);

  // let colors = [
  //    fillColor,
  //    chance.pick( ["#000000","#FFFFFF"] )
  // ]
  //
  // colors.push( colors[1]=='#000000' ? 'white' : 'black' )



  fill( splice(colorlib) , layer, stage)

  //const ql = chance.integer({min:4,max: 8})

  //console.log( colorlib.splice(  colorlib.indexOf(fillColor) , 1) );
  // const wd = stage.getWidth()/ql;
  // const wh = (stage.getHeight() - grid.getCoords().canY[2])/ql;
  // for(let x=0;x<ql;x++){
  //   for(let y =0;y<ql;y++){
  //     const rect1 = new Konva.Rect({
  //         x: wd*x,
  //         y: grid.getCoords().canY[2] + wh*y ,
  //         width: stage.getWidth(),
  //         height: stage.getHeight(),
  //         fill: 'black',
  //         opacity: .1+(x+y)/(ql*ql*2.1) //darkness[x>y?x:y]
  //       })
  //       layer.add(rect1)
  //   }
  // }

        const T = new Konva.Text({
          x: -grid.getCoords().pinkX(0,1,0) ,
          y: stage.getHeight() ,
          text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
          fontSize: grid.getCoords().pinkX(2,1,0),
          fontFamily: 'Conv_Proxima Nova Bold',
          fill: fillColor,
          width: stage.getWidth() ,
          name: "title",
          rotation: -90,
          lineHeight: .5
        })

        layer.add(T)

        const tm = new Konva.Text({
          x: grid.getCoords().canX[0] ,
          y: grid.getCoords().canY[0] ,
          text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
          fontSize: grid.getCoords().height/5,
          fontFamily: 'Conv_Proxima Nova Regular',
          fill: fillColor,
          width: grid.getCoords().width,
          height: grid.getCoords().height,
          name: 'time'
        })

        layer.add(tm)

        const pl = new Konva.Text({
          x: grid.getCoords().canX[1] ,
          y: grid.getCoords().canY[0] ,
          text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
          fontSize: grid.getCoords().height/5,
          fontFamily: 'Conv_Proxima Nova Regular',
          fill: fillColor,
          width: grid.getCoords().width,
          height: grid.getCoords().height,
          name: 'place'
        })

        layer.add(pl)

        const dp = new Konva.Text({
          x: grid.getCoords().canX[2] ,
          y: grid.getCoords().canY[0] ,
          text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
          fontSize: grid.getCoords().height/5,
          fontFamily: 'Conv_Proxima Nova Regular',
          fill: fillColor,
          width: grid.getCoords().width*2,
          height: grid.getCoords().height,
          align: 'right',
          name: 'desc'
        })

        layer.add(dp)
        return layer;
}
