import React from 'react'
import Konva from 'konva'
import Chance from 'chance'
import styles from '../styles.styl'

import draw from '../plakot/'
import router from '../router'

const chance = new Chance();

export default class Ml extends React.Component {
  constructor(){
    super();
    this.state = {
      pl: []
    }
  }
  componentDidMount(){
    const self = this;
    //draw(router.get())
    //setInterval( _=> self.gen() , 1000)
  }
  gen(){
    const im =  draw(this, draw(router.get()) ) ;
    this.setState({
      pl: this.state.pl.concat(im)
    })
  }
  render(){
    const self = this;
    const numbers = [1, 2];
    const bubls = this.state.pl.map((number) =>{
      const i = Math.floor(Math.random()*200000000000000000000);
      const i2 = Math.floor(Math.random()*100);
      return <img key={i} style={{width: '50px', margin: 0, position: 'relative', top: '-2px'}} src={number} />
    });

    return <div className={styles.main} onClick={this.gen.bind(this)}>
      <div style={{width: '80%', margin: '10%'}}>{bubls}</div>
      <div id='plakot' style={{position: 'absolute', top: '100vh'}}></div>
    </div>
  }
}
