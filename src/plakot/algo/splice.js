import Chance from 'chance'
const chance = new Chance()

export default (ar, i=ar.length-1)=>{
  return ar.splice( chance.integer({min:0,max: i}) ,1)[0]
}
