import Konva from 'konva'

export default class Shape {
  constructor(stage, chance, grid, colors){
    const self = this;
    this.stage = stage;
    this.chance = chance;
    this.grid = grid;
    this.colors = colors;
    this.objs = [];
    this.layer = new Konva.Layer();
  }
  draw(){
    const self = this;
    const iter = self.chance.integer({min: 0 , max: 7 });
    const type = self.chance.bool();
    for(let i=0;i<iter;i++){
      const size = //self.chance.pickone(  self.grid.getCoords().canX )/self.grid.getCoords().slice;
      self.chance.integer({min: self.grid.getCoords().slice/50 , max: self.grid.getCoords().slice*40 });
      let opt = {
        x: self.chance.pickone(  self.grid.getCoords().canX ),
        y: self.chance.pickone(  self.grid.getCoords().canY ),
        radius: size
        // stroke: 'black',
        // strokeWidth: 4
      };

      if( self.chance.bool() ){
        opt.fillLinearGradientStartPoint= { x : -size, y : -size};
        opt.fillLinearGradientEndPoint= { x : size, y : size};
        opt.fillLinearGradientColorStops= [0, self.chance.pick(self.colors.get()), 1, self.chance.pick(self.colors.get())];
      }else{
        opt.fill= self.chance.pick(self.colors.get())
      }

      const rect = new Konva.Circle(opt);
      self.objs.push( rect );

    }
    for(var i in this.objs){
      this.layer.add(this.objs[i]);
    }
    return this.layer;
  }
}
