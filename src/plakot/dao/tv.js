// стандартный шаблон плаката

import Konva from 'konva'
import fill from '../algo/fill'

export default function(self, stage, grid, chance){
  const layer = new Konva.Layer();

  let clib = self.colorlib

  const color = clib.splice( chance.integer({min:0,max:1}) ,1)[0]


  fill(clib.splice( chance.integer({min:0,max:clib.length-1}) ,1)[0], layer, stage) //chance.pick( self.colorlib.slice(2, self.colorlib.length) )

  console.log(clib.length);

  const tm = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[0] ,
    text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width*2,
    height: grid.getCoords().height,
    name: 'time'
  })

  layer.add(tm)

  const pl = new Konva.Text({
    x: grid.getCoords().canX[2] ,
    y: grid.getCoords().canY[0] ,
    text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'place'
  })

  layer.add(pl)

  const dp = new Konva.Text({
    x: grid.getCoords().canX[3] ,
    y: grid.getCoords().canY[0] ,
    text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    align: 'right',
    name: 'desc'
  })

  layer.add(dp)

    const Blsh = new Konva.Text({
      x: grid.getCoords().canX[0] ,
      y: grid.getCoords().canY[1] ,
      text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
      fontSize: grid.getCoords().height *2 ,
      fontFamily: 'Conv_Proxima Nova Bold',
      fill: color,
      width: grid.getCoords().width *4 + grid.getCoords().marginX*4,
      height: grid.getCoords().height,
      name: "title"
    })

    layer.add(Blsh)

    for(var i=0;i<5;i++){
      const col = clib.splice( chance.integer({min:0,max:clib.length-1}) ,1)[0]
      console.log(clib.length);
      const rect = new Konva.Rect({
          x: i/5 * stage.getWidth(),
          y: grid.getCoords().canY[2] + grid.getCoords().width *.5,
          width: stage.getWidth()/5,
          height: stage.getHeight(),
          fill: col
      })
      layer.add(rect)
    }



  return layer;

}
