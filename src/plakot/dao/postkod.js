import Konva from 'konva'
import fill from '../algo/fill'

export default (self, stage, grid, chance)=>{
  const layer = new Konva.Layer();


  const colorlib = self.colorlib;
  // step 1
  const fillColor = chance.pick(colorlib);


  let colors = [
     chance.pick(colorlib.slice(2,colorlib.length)),
     chance.pick( ["#000000","#FFFFFF"] )
  ]

  colors.push( colors[1]=='#000000' ? 'white' : 'black' )



  fill(colors[0], layer, stage)

  //console.log( colorlib.splice(  colorlib.indexOf(fillColor) , 1) );
  const rect1 = new Konva.Rect({
      x: 0,
      y: grid.getCoords().canY[2] ,
      width: stage.getWidth(),
      height: stage.getHeight(),
      fill: colors[2] //chance.pick( colorlib.splice(2) )
    })
    layer.add(rect1)

  const rect2 = new Konva.Rect({
      x: grid.getCoords().canX[0] + grid.getCoords().width,
      y: grid.getCoords().canY[2] ,
      width: stage.getWidth() - grid.getCoords().width - grid.getCoords().mrX,
      height: grid.getCoords().height*4.5 + grid.getCoords().marginY*4 + grid.getCoords().mrY ,
      fill: colors[1]
    })
    layer.add(rect2)

    const rect3 = new Konva.Rect({
        x: grid.getCoords().canX[0] + grid.getCoords().width,
        y: grid.getCoords().canY[3] + grid.getCoords().height/2,
        width: grid.getCoords().width*2 + grid.getCoords().marginX*3 ,
        height: grid.getCoords().height*3 + grid.getCoords().marginY*3 + grid.getCoords().mrY ,
        fill: colors[0]
      })
      layer.add(rect3)

      const rect4 = new Konva.Rect({
          x: grid.getCoords().canX[3] ,
          y: grid.getCoords().canY[6] + grid.getCoords().height/2 + grid.getCoords().mrY -2 ,
          width: grid.getCoords().width + grid.getCoords().mrX ,
          height: grid.getCoords().height *10 ,
          fill: colors[1]
        })
        layer.add(rect4)

        const T = new Konva.Text({
          x: grid.getCoords().canX[0] ,
          y: grid.getCoords().canY[1] ,
          text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
          fontSize: grid.getCoords().height/2,
          fontFamily: 'Conv_Proxima Nova Bold',
          fill: colors[2],
          width: grid.getCoords().width*4 + grid.getCoords().marginX*3,
          name: "title"
        })

        layer.add(T)

        const tm = new Konva.Text({
          x: grid.getCoords().canX[0] ,
          y: grid.getCoords().canY[0] ,
          text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
          fontSize: grid.getCoords().height/5,
          fontFamily: 'Conv_Proxima Nova Regular',
          fill: colors[2],
          width: grid.getCoords().width,
          height: grid.getCoords().height,
          name: 'time'
        })

        layer.add(tm)

        const pl = new Konva.Text({
          x: grid.getCoords().canX[1] ,
          y: grid.getCoords().canY[0] ,
          text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
          fontSize: grid.getCoords().height/5,
          fontFamily: 'Conv_Proxima Nova Regular',
          fill: colors[2],
          width: grid.getCoords().width,
          height: grid.getCoords().height,
          name: 'place'
        })

        layer.add(pl)

        const dp = new Konva.Text({
          x: grid.getCoords().canX[2] ,
          y: grid.getCoords().canY[0] ,
          text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
          fontSize: grid.getCoords().height/5,
          fontFamily: 'Conv_Proxima Nova Regular',
          fill: colors[2],
          width: grid.getCoords().width*2,
          height: grid.getCoords().height,
          align: 'right',
          name: 'desc'
        })

        layer.add(dp)
        return layer;
}
