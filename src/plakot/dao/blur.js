// стандартный шаблон плаката

import Konva from 'konva'
import fill from '../algo/fill'

export default function(self, stage, grid, chance){
  const layer = new Konva.Layer();

  const color = chance.pick(["black", "white"])


  fill( chance.pick( self.colorlib.slice(2, self.colorlib.length) ) , layer, stage)

  const tm = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[0] ,
    text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'time'
  })

  layer.add(tm)

  const pl = new Konva.Text({
    x: grid.getCoords().canX[1] ,
    y: grid.getCoords().canY[0] ,
    text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'place'
  })

  layer.add(pl)

  const dp = new Konva.Text({
    x: grid.getCoords().canX[2] ,
    y: grid.getCoords().canY[0] ,
    text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width*2,
    height: grid.getCoords().height,
    align: 'right',
    name: 'desc'
  })

  layer.add(dp)

  for(let i=0;i<80*grid.getCoords().marginY/2;i+= grid.getCoords().marginY/2 ){
    const Blsh = new Konva.Text({
      x: grid.getCoords().canX[0] ,
      y: grid.getCoords().canY[1] + i ,
      text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
      fontSize: grid.getCoords().height *2.5 ,
      fontFamily: 'Conv_Proxima Nova Bold',
      fill: color,
      opacity: i == 2*grid.getCoords().marginY ? 1 : .1,
      width: grid.getCoords().width *4 + grid.getCoords().marginX*4,
      height: grid.getCoords().height,
      name: "title"
    })

    layer.add(Blsh)
  }



  return layer;

}
