import axios from 'axios'

// CONSOLE ART

const instance = axios.create({
  baseURL: '/'//"http://plakot.ru" //,
  //timeout: 2500
  //headers: {'Access-Control-Allow-Origin': '*'}
});

export default class Rest {
  get(id){
    return instance.get('/posters/'+id)
  }
  add(data){
    if(data)
    return instance.post('/add', data)
    else return false;
  }
  order(data){
    if(data)
    return instance.post('/order', data)
    else return false;
  }
  clean(q){
    return instance.get('/clean')
  }
  done(hash){
    return instance.get('/knok_pay_done?shp_hash='+hash)
  }
  stripe(token){
    return instance.post('/stripe', token)
  }
}
