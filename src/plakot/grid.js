import Konva from 'konva'

export default class Grid {
  constructor(stage, chance) {
    const self = this;
    this.stage = stage;
    this.layer = new Konva.Layer();
    this.padding = 3;
    this.column = 10;
    this.visible = true;
    this.slice = 9;
    this.color = chance.color();
    this.point = [self.stage.getWidth()/self.slice , self.stage.getHeight()/self.slice ]
    //[  (1 - 1 / Math.sqrt(2)) * this.stage.getWidth() / 3 , (1 - 1 / Math.sqrt(2)) * this.stage.getHeight() / 3 ]

  }
  setvisible(bool =!this.visible){
    this.visible = bool;
    this.layer.visible( this.visible);
  }
  getCoords(){
    const self = this;
    let canX=[],canY = [];

    const rw = self.stage.getWidth()*.2;
    const rh = self.stage.getHeight()*.1;

    const mrX = self.stage.getWidth()*.04;
    const mrY = self.stage.getHeight()*.03;

    const qlX = parseInt( (self.stage.getWidth() - mrX*2) / rw )
    const ostX = (self.stage.getWidth() - mrX*2) - rw*qlX
    const marginX = self.stage.getWidth()*.04;//ostX/(qlX+1)

    const qlY = parseInt( (self.stage.getHeight() - mrY*2) / rh )
    const ostY = (self.stage.getHeight() - mrY*2) - rh*qlY
    const marginY = self.stage.getHeight()*.02;//ostY/(qlY+1)

    for(var x=0;x<qlX;x++){
      canX.push(  rw*x + marginX*x + mrX  )
    }

    for(var y=0;y<qlY-1;y++){
      canY.push(  rh*y + marginY*y + mrY )
    }

    return {
      start: self.point,
      slice: self.slice,
      width: rw,//self.stage.getWidth() - 2*self.point[0],
      height: rh,
      marginX,
      marginY,
      mrX,
      mrY,
      canX,
      canY,
      pinkX: (a,b,c)=> {return rw*a+marginX*b+mrX*c},
      pinkY: (a,b,c)=> {return rh*a+marginY*b+mrY*c}
    }
  }
  draw(){
    const self = this;
    const fill = this.color;
    let objs = [];

    // for(var n=self.slice;n>0;n--){
    //   objs.push( new Konva.Line({
    //       points: [ n*self.stage.getWidth()/self.slice, 0, n*self.stage.getWidth()/self.slice, self.stage.getHeight() ],
    //       stroke: fill,
    //       strokeWidth: 1
    //   }) )
    //   objs.push( new Konva.Line({
    //       points: [ 0, n*self.stage.getHeight()/self.slice, self.stage.getWidth() , n*self.stage.getHeight()/self.slice ],
    //       stroke: fill,
    //       strokeWidth: 1
    //   }) )
    // }

    const rw = self.stage.getWidth()*.2;
    const rh = self.stage.getHeight()*.1;

    const mrX = self.stage.getWidth()*.04;
    const mrY = self.stage.getHeight()*.03;

    const qlX = parseInt( (self.stage.getWidth() - mrX*2) / rw )
    const ostX = (self.stage.getWidth() - mrX*2) - rw*qlX
    const marginX = self.stage.getWidth()*.04;//ostX/(qlX+1)

    const qlY = parseInt( (self.stage.getHeight() - mrY*2) / rh )
    const ostY = (self.stage.getHeight() - mrY*2) - rh*qlY
    const marginY = self.stage.getHeight()*.02;//ostY/(qlY+1)

    for(var x=0;x<qlX;x++){
      for(var y=0;y<qlY-1;y++){
        objs.push(
          new Konva.Rect({
            x: rw*x + marginX*x + mrX,
            y: rh*y + marginY*y + mrY,
            width: rw,
            height: rh,
            stroke: 'blue'
          })
        )
      }
    }

    for(var i in objs){
      this.layer.add(objs[i]);
    }
    return this.layer;
  }
}
