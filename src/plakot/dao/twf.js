// стандартный шаблон плаката

import Konva from 'konva'
import fill from '../algo/fill'
import splice from '../algo/splice'

//import pattern from '../../pic/logo.png'

export default function(self, stage, grid, chance){
  const layer = new Konva.Layer();

  let clib = self.colorlib

  const color = splice(clib,1)


  fill(clib.splice( chance.integer({min:0,max:clib.length-1}) ,1)[0], layer, stage) //chance.pick( self.colorlib.slice(2, self.colorlib.length) )

  let F = new Konva.Text({
    x: 0 ,
    y: -grid.getCoords().height*2-grid.getCoords().pinkY(0,1,1) ,
    text: self.title.toLowerCase().substring(0,1) ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: stage.getHeight()*1.5 ,
    fontFamily: 'Conv_Proxima Nova Bold',
    fill: splice(clib),
    align: "center",
    name: "title",
    opacity: .6
  })

  layer.add(F)

  F = new Konva.Text({
    x: -grid.getCoords().width*.5-grid.getCoords().pinkX(0,1,1) ,
    y: -grid.getCoords().height*2-grid.getCoords().pinkY(0,0,1) ,
    text: self.title.toLowerCase().substring(1,2) ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: stage.getHeight()*1.6 ,
    fontFamily: 'Conv_Proxima Nova Bold',
    fill: splice(clib),
    align: "center",
    name: "title",
    opacity: .6
  })

  layer.add(F)

  const tm = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[5] ,
    text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'time'
  })

  layer.add(tm)

  const pl = new Konva.Text({
    x: grid.getCoords().canX[1] ,
    y: grid.getCoords().canY[5] ,
    text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width*2,
    height: grid.getCoords().height,
    name: 'place'
  })

  layer.add(pl)

  const dp = new Konva.Text({
    x: grid.getCoords().canX[3] ,
    y: grid.getCoords().canY[5] ,
    text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    align: 'right',
    name: 'desc'
  })

  layer.add(dp)

    const Blsh = new Konva.Text({
      x: grid.getCoords().canX[0] ,
      y: grid.getCoords().canY[4] + grid.getCoords().height/2,
      text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
      fontSize: grid.getCoords().height/2 ,
      fontFamily: 'Conv_Proxima Nova Bold',
      fill: color,
      width: stage.getWidth(),
      height: stage.getWidth(),
      name: "title"
    })

    layer.add(Blsh)

    // if(chance.bool()*chance.bool()*chance.bool()){
    //   let konvaObject = new Konva.Circle({
    //     	x: stage.getWidth()/2,
    //     	y: stage.getWidth()/2,
    //     	radius: stage.getWidth()/2
    //     });
    //   let imageObj = new Image();
    //   imageObj.onload = function () {
    //   	konvaObject.fillPatternImage(imageObj);
    //     konvaObject.setFillPatternOffset({x : 0, y :0});
    //     konvaObject.setFillPatternScale({x:0.1,y:.1});
    //   	konvaObject.draw();
    //   };
    //   imageObj.src = pattern;
    //
    //
    //   // add object to layer
    //   layer.add(konvaObject);
    // }





  return layer;

}
