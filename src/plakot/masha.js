import convert from 'color-convert'


export default class Pallete {
  constructor(color){
    this.color = color;
  }
  _mod(col , n){
    let ncolor = convert.hex.cmyk(col)
    ncolor[n] = ncolor[n]?ncolor[n]*1.5:30;
    console.log(n);
    return convert.cmyk.hex(ncolor);
  }
  get(){
    let r = [];
    for(let i = 0; i< 7; i++){
      if(i){
        r.push( this._mod( r[i -1]  , i%3 ) )
      }else{
        r.push(this.color);
      }
    }
    return r;
  }
}
