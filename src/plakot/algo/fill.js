import Konva from 'konva'

export default (fill , layer, stage)=>{
  const rect = new Konva.Rect({
      x: 0,
      y: 0,
      width: stage.getWidth(),
      height: stage.getHeight(),
      name: 'background',
      fill:fill
    })
  layer.add(rect)
}
