import mongoose from 'mongoose'

class Canvas extends mongoose.Schema {
  constructor() {
    super({
      name: String,
      data: object,
      time: String
    })
  }
}
class History extends mongoose.Schema {
  constructor() {
    super({
      name: String,
      change: object,
      id: String,
      operation: String
    })
  }
}

const canvas = mongoose.model('Canvas', new Canvas);
const history = mongoose.model('Canvas', new History);

export default class DB {
  constructor(name){
    this.name = name;
    this.data = {};
  }
  get(){
    return this.data;
  }
  update(id , params){
    if(id){
      if(this.data[id]){
        params ?
          this.data[id] = { ...this.data[id], params}:
          this._delete(id);
      }
    }else{
      this._add(params);
    }
  }
  _add(params){

  }
  _delete(params){

  }
}
