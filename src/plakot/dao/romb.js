import Konva from 'konva'
import fill from '../algo/fill'

export default function(self, stage, grid, chance){
  const layer = new Konva.Layer();

  const color = chance.pick(["black", "white"])

  const pallete = self.colorlib.slice(2, self.colorlib.length);

  const backgroundColor = chance.pick( pallete );
  let foregroundColor = chance.pick( pallete );

  foregroundColor = backgroundColor == foregroundColor  ? 'white': foregroundColor;
  let foregroundColor1 = "red"//chance.pick( pallete );


  console.log(backgroundColor + ' ' + foregroundColor );

  fill(backgroundColor, layer, stage)

  //const col = chance.pick( self.colorlib.slice(2, self.colorlib.length) );


  let group = new Konva.Group({
    rotation: 45,
    x: stage.getWidth()/2 + grid.getCoords().width/2,//grid.getCoords().canX[0] + grid.getCoords().marginY/2  + k ,
    y: -grid.getCoords().width
  });

  for(var i=8;i>1;i--){
    const wd = (grid.getCoords().width*4 + grid.getCoords().marginX*3 - grid.getCoords().marginY) ;
    const size = 1;
    const k = i*wd/17;
    const rect1 = new Konva.Rect({
        x: grid.getCoords().canX[0] + grid.getCoords().marginY/2  + k ,
        y: grid.getCoords().canY[0] + grid.getCoords().marginY/2 + k ,
        width: wd/size - 2*k ,
        height: wd/size - 2*k ,
        stroke: 'white',//foregroundColor ,
        fill: 'rgba(0,0,0,0)',
        strokeWidth: grid.getCoords().width/(size*8)
    })
    group.add(rect1)
  }
  layer.add(group)


    group = new Konva.Group({
      rotation: 45,
      x: stage.getWidth()/4 + grid.getCoords().width/2,//grid.getCoords().canX[0] + grid.getCoords().marginY/2  + k ,
      y: -grid.getCoords().width,
      opacity: .7
    });

    for(var i=9;i>1;i--){
      const wd = (grid.getCoords().width*4 + grid.getCoords().marginX*3 - grid.getCoords().marginY) ;
      const size = 1;
      const k = i*wd/17;
      const rect1 = new Konva.Rect({
          x: grid.getCoords().canX[0] + grid.getCoords().marginY/2  + k ,
          y: grid.getCoords().canY[0] + grid.getCoords().marginY/2 + k ,
          width: wd/size - 2*k ,
          height: wd/size - 2*k ,
          stroke: foregroundColor ,
          fill: 'rgba(0,0,0,0)',
          strokeWidth: grid.getCoords().width/(size*8)
      })
      group.add(rect1)
    }
    layer.add(group)




  const tm = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[7] ,
    text: self.time ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'time'
  })

  layer.add(tm)

  const pl = new Konva.Text({
    x: grid.getCoords().canX[1] ,
    y: grid.getCoords().canY[7] ,
    text: self.place ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width,
    height: grid.getCoords().height,
    name: 'place'
  })

  layer.add(pl)

  const dp = new Konva.Text({
    x: grid.getCoords().canX[2] ,
    y: grid.getCoords().canY[7] ,
    text: self.desc ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height/5,
    fontFamily: 'Conv_Proxima Nova Regular',
    fill: color,
    width: grid.getCoords().width*2 ,//+ grid.getCoords().magrinX ,//+ grid.getCoords().mrX ,
    height: grid.getCoords().height,
    name: 'desc'
    //align: 'right'
  })

  layer.add(dp)

  const T = new Konva.Text({
    x: grid.getCoords().canX[0] ,
    y: grid.getCoords().canY[5] + grid.getCoords().height/2 + grid.getCoords().mrY ,
    text: self.title.toLowerCase() ,//self.desc.replace( /\s\s+/g, ' ' ).slice(60, self.desc.replace( /\s\s+/g, ' ' ).length ), /// !!! тут в props опрокидываем текст
    fontSize: grid.getCoords().height,
    fontFamily: 'Conv_Proxima Nova Bold',
    fill: color,
    width: grid.getCoords().width*4 + grid.getCoords().marginX*3,
    name: "title"
  })

  layer.add(T)

  return layer;
}
