import React from 'react';
import styles from './styles.styl';
import draw from './plakot/'
import is from 'is_js';
import router from './router';
import axios from 'axios';
import strings from './language';
import limits from './limits'
import store from './store/'


import Mbl from './mobile'
import Order from './components/order'
import Ml from './ml/'


import logo from './pic/logo-11.png'
import bubl from './pic/bubl.png'
import lfing from './pic/lfing.svg'
import lapa from './pic/lapa.svg'
import hamb from './pic/hamb.png'

import bck from './pic/6-06.png'
import nxt from './pic/next.png'

import Rest from './rest'

const R = new Rest()


class View extends React.Component {
  static PropTypes = {
    draw: React.PropTypes.func.isRequired,
    back: React.PropTypes.func.isRequired,
    wanna: React.PropTypes.func.isRequired,
    change: React.PropTypes.func.isRequired,
    state: React.PropTypes.object.isRequired
  }
  constructor(props) {
    super(props);
    this.state = {
      input: "",
      input: styles.input,
      valueTITLE: "",
      valueDESC: "",
      valueSP: ""
    }
  }
  render(){
    const self = this;
    const state = this.props.state;
    return(
      <div className={styles.result}>
        <div className={styles.control}>
          <div className={styles.bottom}>
            <img src={strings.logo} className={styles.logo} />
            <div className={styles.afterlogo}>
              <input value={this.state.valueTITLE} className={this.state.input } onChange={ (e)=> { self.setState({ valueTITLE: e.target.value , input: styles.input }); self.props.change({title: e.target.value }) } } placeholder={strings.eventname} />
              <input value={this.state.valueTIME} className={this.state.input+ ' '+styles.makenorm} onChange={ (e)=> { self.setState({ valueTIME: e.target.value , input: styles.input }); self.props.change({time: e.target.value }) } } placeholder={strings.eventtime} />
              <input value={this.state.valuePLACE} className={this.state.input+ ' '+styles.makenorm} onChange={ (e)=> { self.setState({ valuePLACE: e.target.value , input: styles.input }); self.props.change({place: e.target.value }) } } placeholder={strings.eventplace} />
              <textarea style={{position: 'relative', top: '-4px'}} defaultValue={this.state.valueDESC} className={this.state.input} onChange={ (e)=> { self.setState({ valueDESC: e.target.value , input: styles.input }); self.props.change({desc: e.target.value })} } placeholder={strings.eventdesc}></textarea>
            </div>
          </div>
        </div>
        <div className={styles.plakot} id='plakot'>
          <div className={styles.close} onClick= { this.props.draw }>✕</div>
          <h1>{strings.name}</h1>
          <p>{strings.tizer}</p>
          <img src={lapa} className={styles.lapa} />
        </div>
        <div className={styles.action} id='action' style={{ display: state.firstcall ? 'block' : 'none' }}>
          <div style={{background: `${state.button}`, color: 'white'}} id='wanna' className={ styles.wanna } onClick = {this.props.wanna} >{strings.wanna}</div>
          <div className={styles.navbar}>
            <div onClick= { this.props.back } style={{ opacity: self.props.can.length ? 1 : 0 }} className={styles.bck}>{strings.back}</div>
            <div onClick= { this.props.draw } className={styles.nxt}>{strings.next}</div>
          </div>
        </div>
      </div>
    )
  }
}



export default class App extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      inter: null,
      step: 0,
      title: strings.name,
      desc: 'генератор для вашего события',
      time: 'время',
      place: 'место',
      order: false,
      link: '',
      history: [],
      hamburger: false,
      button: 'black',
      firstcall: false,
      src: '',
      plak: true
    };
  }
  handleImageLoaded() {
    this.setState({ imageStatus: 'loaded' });
  }

  handleImageErrored() {
    this.setState({ imageStatus: 'failed to load' });
  }
  componentDidMount(){
    const self = this;
    R.clean().then(e=> console.log(e.data));
    if(router.get().shp_hash){ R.done(shp_hash) }
    if(router.get().seed ){
      // сомнительное удовольствие
      R.get(router.get().seed).then(e=>{
        if(e.data.error){
          self.setState({title: "404", desc: strings.notfound }, _=> self.historySave( draw(self.state) ) )
        }else{
          draw(this.state, e.data )
        }
      }).catch(e=> {
        console.error(e);
        self.setState({title: "error", desc: e }, _=> self.historySave( draw(self.state) ) )
      })
    }else{
      //draw(this)
    }

    if( router.get().lang ){
      strings.setLanguage( router.get().lang );
    }else{
      router.put({lang: strings.getLanguage() })
    }
  }
  firstdraw(){
    const self = this;
    if(this.state.firstcall){

    }else{
      this.setState({firstcall: true}, _=> self.makeDraw() )
    }
  }
  redraw(json){ // а тут передать параметры из history
    // тут по идее плакаты с отменой заказа
    const self = this;
    const ex = draw( self.state , json ) // ДА сучка!! Он же возвращает false, если в него впихнули json и json, если сделал его сам
    console.log(ex);
    ex ?
    R.add( ex ).then((d)=> {d.status==200 ? router.put({seed: d.data.name.split('.')[0] }) : void(0) }).catch(e=> console.log(e))
    :void(0);
  }
  componentDidUpdate(prevProps,prevState){
    //console.log("did");
    if(prevState.order && !this.state.order ){
      draw( this.state, this.state.history[this.state.history.length - 1 ].data )
    }
    if( // Миха сказал, Арс сделал
      prevState.desc && this.state.desc == ''   ||
      prevState.title && this.state.title == '' ||
      prevState.time && this.state.time == ''   ||
      prevState.place && this.state.place == ''
    ){
      this.changeStep(this.state)
    }
    //this.save = setTimeout( this.redraw , 2500)
  }
  componentWillUpdate(nextProps,nextState){
    //console.log("will");
    // а тут надо запухнуть в history
    //clearTimeout(this.save)
  }
  changeStep(args){
    const self = this;
    let history = this.state.history;
    let children = history[history.length-1].data.children[0].children
    for(var i=0;i<children.length;i++){
      if(children[i].attrs.name == 'title'){
        let t = args.title || self.state.title;
        //t = t.substring(0,limits.lentitle)
        children[i].attrs.text = t
      }
      if(children[i].attrs.name == 'desc'){
        children[i].attrs.text = args.desc || self.state.desc
      }
      if(children[i].attrs.name == 'time'){
        children[i].attrs.text = args.time || self.state.time
      }
      if(children[i].attrs.name == 'place'){
        children[i].attrs.text = args.place || self.state.place
      }
      if(children[i].attrs.name == 'background'){
        args.button = children[i].attrs.fill
      }
    }
    history[history.length-1].data.children[0].children = children
    args.history = history
    this.setState(args, _=>{
      //this.setState({
        draw(this.state, this.state.history[this.state.history.length-1].data)
      //});
    })
  }
  historySave(dt){ // сохрняем в локальную историю
    console.log("call");
    const newObj = { name:   Math.floor( Math.random() * 10 ) + '0' + (new Date()).getTime()  , data: dt}

    let children = dt.children[0].children;
    let bthc = 'black';
    for(var i=0;i<children.length;i++){
      if(children[i].attrs.name == 'background'){
         bthc =   children[i].attrs.fill;
      }
    }

    this.setState({
      history : this.state.history.concat(newObj),
      button: bthc
    })
    return newObj.name;
  }
  makeDraw(){
    const self = this;
    router.put({seed: self.historySave( draw(self.state) ) })
  }
  makeBack(){
    const self = this;
    console.log( this.state.history );
    if(self.state.history.length > 0){
      window.history.go(-1)
      self.setState({
        history: self.state.history.slice(0, self.state.history.length -1 )
      }, _=>  draw( self.state, self.state.history[self.state.history.length - 1 ].data ) )
    }else{
      console.log("no data");
    }
  }
  wanna(st){
    const self = this;
    this.setState({order: st , link:  window.location.href, src: st ? draw(self.state, self.state.history[self.state.history.length - 1 ].data) : '' }, _=> {
      if(st){
        try{
          //мутим отчеты для ананлитики
          yaCounter42977284.reachGoal('wanna');
          ga('send', 'event', 'button', 'submit')
        }catch(e){
          console.error(e);
        }
        // тут мы сохраняем последний плакот из истории в бд
        self.state.history.length ?
        R.add( self.state.history[self.state.history.length - 1 ].data )
         .then((d)=> {d.status==200 ? router.put({seed: d.data.name.split('.')[0] }) : void(0) })
         .catch(e=> console.log(e))
         : void(0);
      }else{
        // тут просто отрисовываем последний плакот из истории
        // но тут проблема!!! компонент ещё не подъехал, куда пихать json, нужно что-то придумать
        // даже в колбеке проблема осталась... компонент ещё не успел смонтироваться
        //self.redraw(self.state , self.state.history[self.state.history.length - 1 ].data )
        // тут пиздато было бы вернуть обещание в компонент didUpdate
      }
    })
    router.get().paid ? window.location.href = window.location.href.split('?')[0] : void(0);
  }
  feedBack(mes){
    if(mes=='sucsess'){
      return strings.thanks
    }else{
      return strings.fail
    }
  }
  render(){
    const self = this;
    const elem =
      <div onClick={this.firstdraw.bind(this)}>
        <div className={styles.content}>
          { router.get().plak ? <Ml /> : router.get().paid ?
            <div className={styles.center}>
              <h1>{this.feedBack( router.get().paid )}</h1>
              <a className={styles.bth} onClick={this.wanna.bind(this, false)}>хочу ещё</a>
            </div>
            : this.state.order ?
            <Order src={this.state.src} back={this.wanna.bind(this, false)} link={this.state.link} border={this.state.button} />
            :
            <View
              draw={this.makeDraw.bind(this) }
              back={this.makeBack.bind(this)}
              can={ this.state.history }
              wanna={this.wanna.bind(this, true)}
              change={this.changeStep.bind(this)}
              state={this.state}
            />
          }
        </div>
      </div>
    return(
      <div>
        { is.mobile() ? <Mbl /> : elem }
      </div>
    )
  }
}
