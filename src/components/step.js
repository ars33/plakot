import React from 'react'
import styles from '../styles.styl'

export default class Step extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      input: "",
      input: styles.input,
      valueTITLE: "",
      valueDESC: "",
      valueSP: "",
      files: "",
      logo: []
    }
  }
  fileUpload(e){
    const self = this;
    for(let f in e.target.files){
      let reader = new FileReader();
      let file = e.target.files[f]
      reader.onload = function(event){
        if(file.type.split('/')[0]=='image'){
          self.setState({
              logo: self.state.logo.concat(event.target.result)
          })
        }
  			//$('#name').html("<b>Name:</b> " + file.name)//$('#size').html(" <b>Size:</b> " + fileSize(file.size, "MB"))
  		}
      reader.readAsDataURL(file);
    }
  }
  render(){
    const self = this;
    const images = self.state.logo.map((link) =>
      <img style={{width: '20px'}} key={Math.random()} src={link}  />
    );
    this.elems = [
      <div>
        <input value={this.state.valueTITLE} className={this.state.input} onChange={ (e)=> { self.setState({ valueTITLE: e.target.value , input: styles.input }); self.props.change({title: e.target.value }) } } placeholder="название мероприятия" />
        <br/>
        <textarea style={{position: 'relative', top: '-4px'}} defaultValue={this.state.valueDESC} className={this.state.input} onChange={ (e)=> { self.setState({ valueDESC: e.target.value , input: styles.input }); self.props.change({desc: e.target.value })} } placeholder="детали"></textarea>
      </div>,
      <div>
        <div style={{width: '200px'}}>{images}</div>
        <br/>
        <small>загрузите логотип</small>
        <input type="file" value={this.state.files} className={this.state.input} onChange={ this.fileUpload.bind(this) } multiple />
        <br/>
        <small>введите описание спонсоров</small>
        <textarea value={this.state.valueSP} onChange={ (e)=> { self.setState({ valueSP: e.target.value}) }} className={this.state.input} placeholder="спонсоры"></textarea>
      </div>
    ]
    let el = <div></div>;
    switch (this.props.step) {
      case 0:
        el = this.elems[0]
        break;
      case 1:
        el = this.elems[1]
        break;
      default:
        el = <div>test</div>
    }
    return(
      <div className={styles.afterlogo}>
        <input value={this.state.valueTITLE} className={this.state.input } onChange={ (e)=> { self.setState({ valueTITLE: e.target.value , input: styles.input }); self.props.change({title: e.target.value }) } } placeholder="название мероприятия" />
        <input value={this.state.valueTIME} className={this.state.input+ ' '+styles.makenorm} onChange={ (e)=> { self.setState({ valueTIME: e.target.value , input: styles.input }); self.props.change({time: e.target.value }) } } placeholder="время" />
        <input value={this.state.valuePLACE} className={this.state.input+ ' '+styles.makenorm} onChange={ (e)=> { self.setState({ valuePLACE: e.target.value , input: styles.input }); self.props.change({place: e.target.value }) } } placeholder="место" />
        <textarea style={{position: 'relative', top: '-4px'}} defaultValue={this.state.valueDESC} className={this.state.input} onChange={ (e)=> { self.setState({ valueDESC: e.target.value , input: styles.input }); self.props.change({desc: e.target.value })} } placeholder="дополнительня информация"></textarea>
      </div>
    )
  }
}
