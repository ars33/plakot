import Konva from 'konva'


export function rbstriangle(x,y,c,fill, vv, vvv ){
  var self = this;
  let v = vv || 0;
  let vx = [
    [x,x+.5*c,x+c],
    [x +c, x,x+c],
    [x +c, x  + .5*c ,x],
    [x, x + c, x]
  ]
  let vy = [
    [y,y+c,y],
    [y,y+.5*c,y+c],
    [ y+c,y,y+c],
    [y+c,y+.5*c,y]
  ]
  return  new Konva.Shape({
    sceneFunc: function(context) {
      context.beginPath();
      context.lineWidth=vvv || 1;
      context.strokeStyle=fill;
      context.moveTo(  vx[v][0],   vy[v][0] );
      context.lineTo(  vx[v][1] ,  vy[v][1] );
      context.lineTo(  vx[v][2],   vy[v][2] );
      context.closePath();
      context.stroke()
      //context.strokeWidth(12);
      //context.fillStrokeShape(this);
    },
    fill: fill
  });
}


export function rbtriangle(x,y,c,fill, vv ){
  var self = this;
  let v = vv || 0;
  let vx = [
    [x,x+.5*c,x+c],
    [x +c,x,x+c],
    [x +c,x  + .5*c,x],
    [x,x + c,x]
  ]
  let vy = [
    [y,y+c,y],
    [y,y+.5*c,y+c],
    [y+c,y,y+c],
    [y+c,y+.5*c,y]
  ]
  return  new Konva.Shape({
    sceneFunc: function(context) {
      context.beginPath();
      context.moveTo(  vx[v][0],   vy[v][0] );
      context.lineTo(  vx[v][1] ,  vy[v][1] );
      context.lineTo(  vx[v][2],   vy[v][2] );
      context.closePath();
      context.fillStrokeShape(this);
    },
    fill: fill
  });
}
export function rtriangle(x,y,c,fill){
  return  new Konva.Shape({
    sceneFunc: function(context) {
      context.beginPath();
      context.moveTo( x, y );
      context.lineTo( x+0.5*c , y + Math.sqrt(.75)*c );
      context.lineTo( x+c, y );
      context.closePath();
      context.fillStrokeShape(this);
    },
    fill: fill
  });
}
export function Rtr(s, x, y, fill){
  return  new Konva.Shape({
    sceneFunc: function(context) {
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x+s, y);
      context.lineTo(x+s, y+s);
      context.closePath();
      context.fillStrokeShape(this);
    },
    fill: fill
  });
  //layer.add( t );
}
export function Ltr(s, x, y, fill){
  return  new Konva.Shape({
    sceneFunc: function(context) {
      context.beginPath();
      context.moveTo(x, y);
      context.lineTo(x+s, y);
      context.lineTo(x, y+s);
      context.closePath();
      context.fillStrokeShape(this);
    },
    fill: fill
  });
  //layer.add( t );
}
export function triangle(s, x, y, fill){
  return  new Konva.Shape({
    sceneFunc: function(context) {
      context.beginPath();
      context.moveTo(x, y+s);
      context.lineTo(x+s, y+s);
      context.lineTo(x+s, y);
      context.closePath();
      context.fillStrokeShape(this);
    },
    fill: fill
  });
  //layer.add( t );
}
